# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The Fuse Project Generator is a Java application that takes a RAML file as input and creates a set of project files that can be 
imported into RH JBoss Fuse.  The project stubs out all REST API endpoints defined in the RAML using schemas and examples
(if available).  Default error handling is included.

### How do I get set up? ###

The application is written as a Maven-based Eclipse project.  Clone the whole project into your local sandbox and import it
as a Maven project into your Eclipse environment.

### Contribution guidelines ###

Changes or improvements to this application should be made on a feature or defect branch.  For example, "FEATURE-Trait-Support" 
or "DEFECT-Error-Handler-Broken".  Later, we might use Jira to track requests, features, and bugs.

### Who do I talk to? ###

Contact Mark Norton at mnorton@ms3-inc.com
